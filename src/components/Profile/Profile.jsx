import React from 'react';

import ProfileList from '../../shared/components/ProfileList';
import './Profile.scss'

const Profile = () => {
    return (
        <div>
            <ProfileList />
        </div>
    );
};

export default Profile;
