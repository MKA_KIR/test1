import React from 'react';
import {useSelector, shallowEqual} from "react-redux";

const LoginPage = () => {
    const {profileData} = useSelector(state=> ({profileData: state.profileData}), shallowEqual)
    return (
        <div>
            <h2>User name: {profileData.name}</h2>
            <h2>User second name: {profileData.secondName}</h2>
            <h2>User email: {profileData.email}</h2>
        </div>
    );
};

export default LoginPage;
