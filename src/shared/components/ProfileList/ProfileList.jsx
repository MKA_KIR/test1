import React, {useState} from 'react';
import {useHistory} from 'react-router-dom'
import {useDispatch} from "react-redux";

import Button from '../Button';
import Input from '../Input';
import {createLogin} from "../../../store/actions";

import './ProfileList.scss'

const ProfileList = () => {
    const [name, setName] = useState('')
    const [secondName, setSecondName] = useState('')
    const [email, setEmail] = useState('')
    const history = useHistory()
    const dispatch = useDispatch()

    const handleSubmit = (e)=>{
        e.preventDefault()
        const profileData = {
            name,
            secondName,
            email
        };
        const loginAction = createLogin(profileData)
        dispatch(loginAction)
        history.push('/login')

    }

    const onNameChange = (value) =>{
        setName(value)
    }

    const onSecondNameChange = (value) =>{
        setSecondName(value)
    }

    const onEmailChange = (value) =>{
        setEmail(value)
    }

    return (
        <form className='profile-list' onSubmit={handleSubmit}>
            <Input type="text" name="name" value={name} placeholder='name' handleChange={onNameChange}/>
            <Input type="text" name="secondName" value={secondName} placeholder='secondName' handleChange={onSecondNameChange}/>
            <Input type="text" name="email" value={email} placeholder='email' handleChange={onEmailChange}/>
            <Button type="primary" text="Got to Vue" />
        </form>
    );
};

export default ProfileList;



// import React, {Component} from 'react';
//
// import './ProfileList.scss'
//
// import Button from '../Button';
// import Input from '../Input'
//
// export class ProfileList extends Component{
//
//     state ={
//         edit:'false',
//         name:'',
//         secondName:'',
//         email:'',
//         placeholder: ''
//     };
//
//     editstate = () =>{
//         const {name, secondName, email} = this.props;
//         this.setState({edit:true, name, secondName, email});
//     };
//
//     finishUpdate = (e) =>{
//         e.preventDefault();
//         const {name, secondName, email} = this.state;
//         this.props.updateProfile(name, secondName, email);
//         this.setState({edit:false});
//     };
//
//
//     handleChange = (event) =>{
//         const obj = {[event.target.name]: event.target.value};
//         this.setState(obj);
//     };
//
//     render() {
//         if (this.state.edit) {
//             const {name, secondName, email} = this.state;
//             return (
//                 <tr>
//                     <td>
//                         <div className="form-group">
//                             <Input type="text" name="name" value={name} placeholder='name' required
//                                    handleChange={this.handleChange}/>
//                         </div>
//                     </td>
//                     <td>
//                         <div className="form-group">
//                             <Input type="text" name="secondName" value={secondName} placeholder='secondName' required
//                                    handleChange={this.handleChange}/>
//                         </div>
//                     </td>
//                     <td>
//                         <div className="form-group">
//                             <Input type="text" name="email" value={email} placeholder='email' required handleChange={this.handleChange}/>
//                         </div>
//                     </td>
//                     <td><Button type="primary" text="Got to Vue" handleClick={this.finishUpdate}/>
//                     </td>
//                 </tr>
//             )
//         } else {
//             const {name, secondName, email} = this.props;
//             return (
//                 <tr>
//                     <td>{name}</td>
//                     <td>{secondName}</td>
//                     <td>{email}</td>
//                 </tr>
//             )
//         }
//     }
// };
