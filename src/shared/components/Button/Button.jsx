import React from 'react';

import {buttonTypes} from "./types";

import './Button.scss'


const Button = ({text, type, handleClick}) => {
    return (
        <div>
            <button onClick={handleClick} className={`btn ${buttonTypes[type]}`}>
                {text}
            </button>
        </div>
    );
};

export default Button;
