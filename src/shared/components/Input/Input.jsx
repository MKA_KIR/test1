import React from 'react';

import './Input.scss'

const Input = ({ name, handleChange, className, type, placeholder, required=false, value}) => {
    const fullClassName = (className) ? `form-control ${className}` : 'form-control';
    const onChange = ({target}) => handleChange(target.value)
    return (
        <div>
            <input name={name} type={type} required={required} value={value}
                   placeholder={placeholder} className={fullClassName} onChange={onChange}/>
        </div>
    );
};

export default Input;
