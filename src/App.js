import React from "react";
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import {Provider} from "react-redux";

import LoginPage from "./components/LoginPage";
import './App.css';

import Profile from "./components/Profile";
import store from "./store/store";

function App() {
    return (
        <Provider store={store}>
            <div className="App">
                <Router>
                    <Switch>
                        <Route exact path="/">
                            <Profile/>
                        </Route>
                        <Route path="/login">
                            <LoginPage />
                        </Route>
                    </Switch>
                </Router>
            </div>
        </Provider>

);
}

export default App;
